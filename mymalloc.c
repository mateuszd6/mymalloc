/* Returns mem_size of virtual memory, aligned to 4MB, uncommited */
static void*
segmt_map(size_t mem_size)
{
    int prot = PROT_READ | PROT_WRITE;
    int flags = MAP_PRIVATE | MAP_ANONYMOUS;
    ptrdiff_t map_size = SEGMENT_SIZE + mem_size;
    void* alloced_mem = mmap(0, map_size, prot, flags, -1, 0);
    void* aligned_mem = (void*) (((size_t) alloced_mem + (SEGMENT_SIZE - 1)) & (~(SEGMENT_SIZE - 1)));
    if (UNLIKELY(alloced_mem == MAP_FAILED))
        return NULL;

    /* TODO: Try to do this differently? Like don't call munmap at all? */
    ptrdiff_t head_offst = aligned_mem - alloced_mem;
    int r1 = munmap(alloced_mem, head_offst);
    (void) r1; /* TODO: Check errno */

    int r2 = munmap(alloced_mem + head_offst + mem_size, map_size - mem_size - head_offst);
    (void) r2; /* TODO: Check errno */

    /* TODO: ? */
    /* madvise(aligned_mem, mem_size, MADV_SEQUENTIAL); */

    return aligned_mem;
}

static page*
segmt_get_small(void)
{
    /* DEBUG */
    heap* h = &heap_local;
    (void) h;

    /* If there are some small pages in cache, use those */
    if (heap_local.small_free)
    {
        page* result = heap_local.small_free;

        heap_local.small_free = heap_local.small_free->next;
        heap_local.small_cached--;
        heap_local.small_curr_used++;
        heap_local.small_max_used += (heap_local.small_curr_used > heap_local.small_max_used);
        assert(heap_local.small_max_used >= heap_local.small_curr_used);

        result->next = result;
        result->prev = result;
        return result;
    }

    /* Otherwise allocate new big segment and divide it into small pages. We
     * will have to use all of these before allocating new segment. */
    void* thread_id = mi_tls_slot(0);
    void* mem = segmt_map(SEGMENT_SIZE); /* TODO: Take from big cache if present? */
    segmt* slab = mem;
    slab->next = (page*) slab; /* This will be changed later */
    slab->prev = (page*) slab; /* This will be changed later */
    slab->shift_to_page = SHIFT_TO_PAGE_SMALL;
    slab->type = SEGMT_TYPE_SMALL;
    slab->heap_id = thread_id;
    slab->pages_in_segment = NUM_PAGES_IN_SEGMENT;

    page* p = (void*) (((char*) mem) + PAGE_SIZE);
    heap_local.small_free = p;
    p = (page*) (((char*) p) + PAGE_SIZE);

    page* head = heap_local.small_free;
    page* end = mem + NUM_PAGES_IN_SEGMENT * PAGE_SIZE;
    for (; p != end; p = (page*) (((char*) p) + PAGE_SIZE))
    {
        head->next = p;
        head = p;
    }

    heap_local.small_cached += NUM_PAGES_IN_SEGMENT - 1;
    heap_local.small_curr_used++;
    heap_local.small_max_used += (heap_local.small_curr_used > heap_local.small_max_used);
    assert(heap_local.small_max_used >= heap_local.small_curr_used);

    return (page*) slab;
}

static void*
segmt_get_large(void)
{
    /* DEBUG */
    heap* h = &heap_local;
    (void) h;

    /* If there are some big pages in cache, use those */
    if (heap_local.big_free)
    {
        page* result = heap_local.big_free;
        heap_local.big_free = heap_local.big_free->next;
        heap_local.big_cached--;
        heap_local.big_curr_used++;
        heap_local.big_max_used += (heap_local.big_curr_used > heap_local.big_max_used);
        assert(heap_local.big_max_used >= heap_local.big_curr_used);

        result->next = (page*) result; /* This will be changed later */
        result->prev = (page*) result; /* This will be changed later */
        /* Those should already be set */
        assert(((segmt*) result)->shift_to_page == SHIFT_TO_PAGE_LARGE);
        assert(((segmt*) result)->type == SEGMT_TYPE_LARGE);
        assert(((segmt*) result)->heap_id == mi_tls_slot(0));
        assert(((segmt*) result)->pages_in_segment == 1); /* TODO: Do we actually need this one? */

        return result;
    }

    void* thread_id = mi_tls_slot(0);
    void* mem = segmt_map(SEGMENT_SIZE);
    segmt* slab = mem;
    slab->next = (page*) slab; /* This will be changed later */
    slab->prev = (page*) slab; /* This will be changed later */
    slab->shift_to_page = SHIFT_TO_PAGE_LARGE;
    slab->type = SEGMT_TYPE_LARGE;
    slab->heap_id = thread_id;
    slab->pages_in_segment = 1; /* TODO: Do we actually need this one? */

    heap_local.big_curr_used++;
    heap_local.big_max_used += (heap_local.big_curr_used > heap_local.big_max_used);
    assert(heap_local.big_max_used >= heap_local.big_curr_used);

    return (page*) slab;
}

static void*
segmt_get_very_large(size_t size)
{
    /* We have to align size of the mmap allocation to the size of the page,
     * otherwise we get weird things */
    size_t aligned_size = (sizeof(segmt) + size + 4095) & (~4095);
    segmt* slab = segmt_map(aligned_size);
    void* thread_id = mi_tls_slot(0);
    slab->next = (page*) slab; /* never used for very large pages */
    slab->prev = (page*) slab; /* never used for very large pages */
    slab->shift_to_page = SHIFT_TO_PAGE_VERY_LARGE;
    slab->type = SEGMT_TYPE_VERY_LARGE;
    slab->heap_id = thread_id;
    slab->size = aligned_size;
    slab->meta[0] = (u64) -1; /* All taken */
    slab->meta[1] = (u64) -1; /* All taken */
    slab->used = 1;

    /* thread_free list is not used for very large pages. These can be safely
     * unmapped by another thread, however we go the long way around to make
     * things easier for the fast path */
    slab->thread_freed = 0;
    slab->thread_free = (void*) NULL;

    return slab;
}

static int
page_is_fully_taken(page* pg)
{
    return pg->meta[0] == (u64) -1;
}

/* Take a single chunk from 64K page. Assumes that there is a free chunk */
static inline void*
page_take(page* pg)
{
    i64 meta_ffusll = ffusll(pg->meta[0]);
    i64 meta_offst = meta_ffusll + 1;
    i64 block_idx = ffusll(pg->meta[meta_offst]);
    pg->meta[meta_offst] |= (1ULL << block_idx);
    pg->meta[0] |= ((u64) (pg->meta[meta_offst] == (u64) -1)) << meta_ffusll;
    pg->used++;

    ptrdiff_t offset = (meta_ffusll * 64 + block_idx) * pg->size;
    return ((char*) pg) + offset;
}

static inline int
page_remove(page* pg, void* ptr)
{
    i64 idx = ((u64) (ptr - (void*) pg)) >> ctz32(pg->size);
    i64 group_idx = idx / 64;
    i64 group_mod = idx % 64;
    i64 meta_group_idx = group_idx + 1;

    /* Disable this bit, because we know there is at least one free slot now */
    pg->meta[0] &= ~(1ULL << group_idx);
    pg->meta[meta_group_idx] &= ~(1ULL << group_mod);
    pg->used--;
    assert(pg->used >= 0);

    /* Try to avoid reading thread_freed if it is not necesarry. Hopefully this
     * gets inlined and compilers handles this correctly for us, as we will
     * probably jump on this result anyway */
    if (UNLIKELY(pg->used - pg->thread_freed == 0))
        return 1;

    return pg->used == 0;
}

/* Sets everything up and returns a new ptr so that no more action is needed in
 * the allocator. Size is actuall size, not (1 << size_l2) so that we can
 * allocate very large pages to exactly the size we need  */
static void*
segmt_allocate_new_page_and_take(i32 size, i32 size_l2)
{
    if (UNLIKELY(size > MAX_LARGE_ALLOC_SIZE))
    {
        return segmt_get_very_large(size) + sizeof(segmt);
    }
    else
    {
        page* pg = size <= MAX_SMALL_ALLOC_SIZE ? segmt_get_small() : segmt_get_large();
        page* curr_head = heap_local.curr_block[size_l2];
        pg->size = 1 << (size_l2 + 4);
        pg->meta[0] = g_bitmeta[size_l2].meta0;
        pg->meta[1] = g_bitmeta[size_l2].meta1;
        pg->used = 0;
        pg->thread_freed = 0;
        pg->thread_free = (void*) NULL;
        heap_local.curr_block[size_l2] = pg;
        if (LIKELY(curr_head != NULL))
        {
            pg->next = curr_head->next; /* Append and rotate */
            pg->prev = curr_head;
            curr_head->next->prev = pg;
            curr_head->next = pg;
        }

        return page_take(pg);
    }
}

/* TODO: The following two have some part in common, try to merge them? */

static inline void
page_free_small(segmt* sg, page* pg)
{
    /* DEBUG */
    heap* h = &heap_local;
    (void) h;

    /* Page is fully freed */
    assert(pg->used == 0);
    assert(pg->thread_freed == 0);

    /* If page we are freeing now is in the curr_block map, don't free it
     * (we will keep at most one page in all-allocate-all-free scenario) */
    page* curr_block_page = heap_local.curr_block[ctz32(pg->size) - 4];
    if (curr_block_page == pg)
        return;

    /* TODO: add minimal treshold to can_cache? */
    b32 can_cache = heap_local.small_cached * MAX_SMALL_FRACT_CACHE < heap_local.small_max_used;
    assert(sg->pages_in_segment >= 1);

    /* Remove from size queue and unmap. At this point, there are at least 2
     * pages so it always reasonable */
    pg->prev->next = pg->next;
    pg->next->prev = pg->prev;

    if (can_cache || ((void*) sg == (void*) pg && sg->pages_in_segment > 1))
    {
        pg->next = heap_local.small_free;
        heap_local.small_free = pg;
        heap_local.small_curr_used--;
        heap_local.small_cached++;
    }
    else
    {
        /* TODO: Don't unmap here? Maybe unmap when all pages from the segment
         * are free? */
        sg->pages_in_segment--;
        heap_local.small_curr_used--;

        munmap(pg, PAGE_SIZE);
    }
}

static inline void
segmt_free(segmt* sg)
{
    /* DEBUG */
    heap* h = &heap_local;
    (void) h;

    /* Page is fully freed */
    page* pg = (page*) sg;
    assert(pg->used - pg->thread_freed == 0);

    /* If page we are freeing now is in the curr_block map, don't free it (we
     * will keep at most one page in all-allocate-all-free scenario) */
    /* TODO: Make sure this is correct for very large pages (curr_block_page
     * should always be NULL) */
    page* curr_block_page = heap_local.curr_block[ctz32(pg->size) - 4];
    if (curr_block_page == pg)
        return;

    /* Remove from size queue and unmap. At this point, there are at least 2
     * pages so it always reasonable */
    pg->prev->next = pg->next;
    pg->next->prev = pg->prev;

    b32 can_cache = heap_local.big_cached * MAX_BIG_FRACT_CACHE < heap_local.big_max_used;
    if (can_cache)
    {
        pg->next = heap_local.big_free;
        heap_local.big_free = pg;
        heap_local.big_curr_used--;
        heap_local.big_cached++;
    }
    else
    {
        heap_local.big_curr_used--;
        munmap(sg, SEGMENT_SIZE);
    }
}

static inline void
segmt_very_large_free(segmt* sg)
{
    /* Make sure we are freeing a large segment here */
    assert(sg->size > MAX_LARGE_ALLOC_SIZE);
    munmap(sg, sg->size);
}

static void*
my_malloc(size_t size)
{
    /* DEBUG */
    heap* h = &heap_local;
    (void) h;

    int size_l2 = next_pow2_u64(((size + 15) >> 4) << 4) - 4;
    page* pg = heap_local.curr_block[size_l2];
    if (LIKELY(pg != NULL) && LIKELY(!page_is_fully_taken(pg)))
        return page_take(pg);

    /* Allocate first page of a given size (or a large page) */
    if (UNLIKELY(!pg))
        return segmt_allocate_new_page_and_take(size, size_l2);

    if (UNLIKELY(page_is_fully_taken(pg))) /* TODO: Less unlikely than the above */
    {
        page* start = pg;
        do
        {
            /* TODO: Extract to a function */
            if (pg->thread_freed > 0)
            {
                void** p = atomic_ptr_swap(&pg->thread_free, NULL);
                i32 freed = 0;
                for (; p; p = *p, freed++)
                {
                    i64 ptr_offset = ((char*) p - (char*) pg) >> (size_l2 + 4);
                    i64 group_idx = ptr_offset / 64;
                    i64 group_mod = ptr_offset % 64;
                    i64 meta_group_idx = group_idx + 1;

                    /* assert that the bit has been set before */
                    assert(pg->meta[meta_group_idx] & (1ULL << group_mod));
                    pg->meta[0] &= ~(1ULL << group_idx);
                    pg->meta[meta_group_idx] &= ~(1ULL << group_mod);
                }

                atomic_sub(&pg->thread_freed, freed);
                break; /* We've found the page with some free space */
            }

            pg = pg->next;
        } while (pg != start && page_is_fully_taken(pg));

        /* No free page found need to allocate new one. Need to check if
         * page is fully taken, because it's possible that we've reclaimed
         * some space from thred_freed list */
        if (UNLIKELY(pg == start) && UNLIKELY(page_is_fully_taken(pg)))
            return segmt_allocate_new_page_and_take(size, size_l2);
    }

    return page_take(pg);
}

static void
my_free(void* ptr)
{
    /* DEBUG */
    heap* h = &heap_local;
    (void) h;

    if (UNLIKELY(ptr == NULL))
        return;

    segmt* sg = (void*) ((size_t) ptr & (~((1 << 22) - 1)));
    page* pg = (void*) ((size_t) ptr & (~((1 << sg->shift_to_page) - 1)));
    void* thread_id = mi_tls_slot(0);

    /* Freeing from a different thread */
    if (UNLIKELY(sg->heap_id != thread_id))
    {
        *(void**) ptr = NULL;
        atomic_push((void*) &pg->thread_free, ptr);
        atomic_inc(&pg->thread_freed);

        return;
    }

    int cleared = page_remove(pg, ptr);
    if (UNLIKELY(cleared))
    {
        switch (sg->type) {
        case SEGMT_TYPE_SMALL:
            return page_free_small(sg, pg);
        case SEGMT_TYPE_LARGE:
            return segmt_free(sg);
        case SEGMT_TYPE_VERY_LARGE:
            return segmt_very_large_free(sg);
        }
    }
}

static void*
my_realloc(void *ptr, size_t size)
{
    if (!ptr)
        return my_malloc(size);

    segmt* sg = (void*) ((size_t) ptr & (~((1 << 22) - 1)));
    page* pg = (void*) ((size_t) ptr & (~((1 << sg->shift_to_page) - 1)));
    if ((ptrdiff_t) size <= (ptrdiff_t) pg->size)
        return ptr;

    void* retval = my_malloc(size);
    memcpy(retval, ptr, pg->size);
    my_free(ptr);

    return retval;
}

static void*
my_calloc(size_t nmemb, size_t size)
{
    /* Correct implementation should check for overflow */
    size_t sz = size * nmemb;
    void* retval = my_malloc(sz);
    memset(retval, 0, sz);

    return retval;
}

static size_t
my_malloc_usable_size(void *ptr)
{
    if (!ptr)
        return 0;

    segmt* sg = (void*) ((size_t) ptr & (~((1 << 22) - 1)));
    page* pg = (void*) ((size_t) ptr & (~((1 << sg->shift_to_page) - 1)));
    return pg->size;
}

static void*
my_aligned_alloc(size_t alignment, size_t size)
{
    void* result = my_malloc(size + (alignment - 1));
    return (void*) ((((size_t) result) + (alignment - 1)) & (~(alignment - 1)));
}

static void*
my_memalign(size_t alignment, size_t size)
{
    return my_aligned_alloc(alignment, size);
}

static int
my_posix_memalign(void **memptr, size_t alignment, size_t size)
{
    void* result = my_aligned_alloc(alignment, size);
    if (LIKELY(result != NULL))
    {
        *memptr = result;
        return 0;
    }
    else
    {
        return ENOMEM;
    }
}
