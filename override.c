static void* my_malloc(size_t size);
static void  my_free(void *ptr);
static void* my_realloc(void *ptr, size_t size);
static void* my_calloc(size_t nmemb, size_t size);
static size_t my_malloc_usable_size(void *ptr);
static void* my_aligned_alloc(size_t alignment, size_t size);
static void* my_memalign(size_t alignment, size_t size);
static int my_posix_memalign(void **memptr, size_t alignment, size_t size);

#ifdef __cplusplus
extern "C" {
#endif

void*
malloc(size_t size)
{
#ifdef DEBUG
    char buf[1024];
    void* ret = my_malloc(size);
    size_t id = (size_t) mi_tls_slot(0);

    snprintf(buf, 1024, "A %zu %zu %zu\n", id, (size_t) ret, size);
    write(2, buf, strlen(buf));
    return ret;
#else
    return my_malloc(size);
#endif
}

void*
calloc(size_t size, size_t n)
{
    return my_calloc(size, n);
}

void*
realloc(void* p, size_t newsize)
{
    return my_realloc(p, newsize);
}

void
free(void* p)
{
#ifdef DEBUG
    char buf[1024];
    size_t id = (size_t) mi_tls_slot(0);

    snprintf(buf, 1024, "F %zu %zu\n", id, (size_t) p);
    write(2, buf, strlen(buf));
    return;
#else
    return my_free(p);
#endif
}

size_t malloc_usable_size(void *ptr)
{
    return my_malloc_usable_size(ptr);
}

void*
aligned_alloc(size_t alignment, size_t size)
{
    return my_aligned_alloc(alignment, size);
}

void*
memalign(size_t alignment, size_t size)
{
    return my_memalign(alignment, size);
}

int
posix_memalign(void **memptr, size_t alignment, size_t size)
{
    return my_posix_memalign(memptr, alignment, size);
}

#ifdef __cplusplus
}
#endif
