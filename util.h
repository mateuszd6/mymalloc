#ifndef UTIL_H_
#define UTIL_H_

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;
typedef float f32;
typedef double f64;
typedef size_t umm;
typedef ptrdiff_t mm;
typedef i8 b8;
typedef i32 b32;

#if (defined(__GNUC__) || defined(__clang__))
#  define LIKELY(EXPR) (__builtin_expect((EXPR), 1))
#  define UNLIKELY(EXPR) (__builtin_expect((EXPR), 0))
#else
#  define LIKELY(EXPR) (EXPR)
#  define UNLIKELY(EXPR) (EXPR)
#endif /* (defined(__GNUC__) || defined(__clang__)) */

#if (defined(__GNUC__) || defined(__clang__))
#  define NOTREACHED() __builtin_unreachable()
#else
#  define NOTREACHED() assert(0)
#endif

#define next_pow2_u64(X) ((int) (64 - __builtin_clzll((X) - 1)))
#define ctz32(X) (__builtin_ctz((X)))

/* If all bits are set, the result is undefined. This is faster that ffs because
 * ffs guarantees the output for edgecases. */
static inline uint64_t
ffusll(u64 n)
{
    assert(n != (u64) -1);
    return __builtin_ctzll(~n);
}

#endif // UTIL_H_
