#define printf(...)
#define fflush(...)
#ifndef DEBUG
#  define NDEBUG
#endif
#include <assert.h>
#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/syscall.h>

#include "mymalloc.h"
#include "third_party/third_party.h"
#include "util.h"

#include "mymalloc.c"
#include "override.c"

#if 0
#ifdef MAIN
int
main(void)
{
#ifdef DEBUG
    heap* dbg_hl = &heap_local;
    (void) dbg_hl;
#endif

    for (int i = 0; i < 2037; ++i)
        (void) my_malloc(24 + (i % 2 == 1));

    for (int i = 0; i < 4063 + 100; ++i)
        (void) my_malloc(1 + 8 * (i % 2 == 1));

    void* p2 = my_malloc(24);
    void* p3 = my_malloc(18);
    void* z1 = my_malloc(10);
    void* z2 = my_malloc(16);
    (void) p2;
    (void) p3;
    (void) z1;
    (void) z2;

    void* p5 = my_malloc(14 * 1024 * 1024);

    my_free(p5);
    /* my_free(p2); */
    /* my_free(p3); */
    /* my_free(p4); */

    return 0;

#define DUMP()                                                                 \
    printf("-------------------------------\n");                               \
    for (int i_ = 0; i_ < 4; ++i_)                                             \
    {                                                                          \
        printf("%02d: ", i_);                                                  \
        print_lsb_first(meta[i_]);                                             \
    }                                                                          \
    printf("--\n");                                                            \
                                                                               \
    for (int i_ = 4; i_ < 32 + 4; ++i_)                                        \
    {                                                                          \
        printf("%02d: ", i_);                                                  \
        print_lsb_first(meta[i_]);                                             \
    } fflush(stdout)

    void* p0 = my_malloc(30);
    u64* meta = (void*) heap_local.curr_block[1].head;
    DUMP();
    my_free(p0);
    DUMP();

    grand_seed(time(0));
    // heap_local_init();

    /* typedef struct alloced_blk alloced_blk; */
    struct alloced_blk { char bytes[32]; };
    struct alloced_blk* ptrs[2039];
    struct alloced_blk alloced_blks[2039];

    // Test
    // u32* data = (void*) curr_block;
    // data[66] = 0b101010010101010101110101010011 | (1ULL << 31);

#define SET_PTR(IDX)                                                           \
    do {                                                                       \
        ptrs[(IDX)] = my_malloc(32);                                           \
        for (int b = 0; b < 32; ++b)                                           \
        {                                                                      \
            ptrs[(IDX)]->bytes[b] = grand_u8();                                \
            alloced_blks[(IDX)].bytes[b] = ptrs[(IDX)]->bytes[b];              \
        }                                                                      \
    } while (0)

#define ASSERT_PTR(IDX)                                                        \
    do {                                                                       \
        for (int b = 0; b < 32; ++b)                                           \
            assert(ptrs[(IDX)]->bytes[b] == alloced_blks[(IDX)].bytes[b]);     \
    } while (0)


    // Init stuff so that we can debug in easier
    // u64* meta;
    {
#if 0
        char* c = my_malloc(30);
        meta = (void*) heap_local.curr_block[1].head;
        DUMP();
        my_free(c);
        DUMP();
#else
        SET_PTR(0);
        meta = (void*) heap_local.curr_block[1].head;
        DUMP();
        my_free(ptrs[0]);
        DUMP();
#endif
    }

#if 1

    for (int i = 0; i < 2039; ++i)
    {
        DUMP();
        SET_PTR(i);
    }

    printf("\n");
    DUMP();
    printf("OK: \n");

    char* p = my_malloc(30);
    meta = (void*) heap_local.curr_block[1].head;
    my_free(p);
    for (int i = 0; i < 2039; ++i)
    {
        DUMP();
        SET_PTR(i);
        // printf("%d: %ld\n", i, (char*) ptrs[i] - (char*) meta);
    }
    DUMP();

    p = my_malloc(30);
    meta = (void*) heap_local.curr_block[1].head;
    DUMP();

    return 0;

    /*
    for (int i = 0; i < 2039; ++i)
        ASSERT_PTR(i);

    for (int r = 0; r < 1019; ++r)
    {
        int idx = grand_u32() % 2039;
        remove_from_block(curr_block, ptrs[idx]);
    }
    printf("\n");
    DUMP();

    for (int r = 0; r < 760; ++r)
    {
        SET_PTR(0);
        // take_from_block(curr_block, 32);
    }
    printf("\n");
    DUMP();
    */

#else
    DUMP();
    void* ptrs[128];
    for (int i = 0; i < 128; ++i)
        ptrs[i] = take_from_block(curr_block, 32);

    remove_from_block(curr_block, ptrs[10]);
    DUMP();
    ptrs[10] = take_from_block(curr_block, 32);
    DUMP();
#endif

    // printf("%ld\n", (u32*) p - (u32*) (&data[0]));
    // printf("%p %p\n", p, &data[66]);
    // assert((void*) p == (void*) (&data[66]));

    // printf("%ld\n", (u32*) take_from_block(curr_block, 32) - (u32*) (&data[0]));
    // printf("%ld\n", (u32*) take_from_block(curr_block, 32) - (u32*) (&data[0]));
    // printf("%ld\n", (u32*) take_from_block(curr_block, 32) - (u32*) (&data[0]));

    return 0;
}
#endif
#endif
