#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "timer.h"

#include "../main.c"

#define NALLOCS 2038
#define NREPS 1 // (128 * 1024)

char volatile* p[NALLOCS];
pthread_t threads[2];

extern void*
free_every_2nd(void* ignore)
{
    (void) ignore;
    for (i64 i = 2; i < 10; i += 2)
        my_free((void*) p[i]);

    printf("Thread 2 done\n");
    return NULL;
}

extern void*
free_every_3rd(void* ignore)
{
    (void) ignore;
    for (i64 i = 3; i < 10; i += 3)
        if (i % 2 != 0)
            my_free((void*) p[i]);

    printf("Thread 3 done\n");
    return NULL;
}

int main()
{
    /* timer t = {0}; */
    /* TIMER_START(t); */
    for (int r = 0; r < NREPS; ++r)
    {
        for (int i = 0; i < NALLOCS; ++i)
        {
            p[i] = my_malloc(30);
            p[i][0] = 'a';
            p[i][2] = 'b';
        }

        pthread_create(threads + 0, 0, free_every_2nd, 0);
        pthread_create(threads + 1, 0, free_every_3rd, 0);
        printf("threads started\n");

#if 0
        for (int i = 0; i < NALLOCS; ++i)
            if (i % 2 != 0 && i % 3 != 0)
                my_free((void*) p[i]);
#endif

        pthread_join(threads[0], NULL);
        pthread_join(threads[1], NULL);

        /* Allocate one more in the main thread to force cleanup? */
        for (int i = 0; i < 6; ++i)
        {
            void* q = my_malloc(26);
            printf("Got ptr: %p\n", q);
            *((char*) q) = 0;
        }
        /* my_free(q); */
    }
    /* TIMER_STOP(t); */
    /* fprintf(stdout, "Elapsed: %.3lfs\n", (double) timer_diff(&t) / (1000 * 1000 * 1000)); */

    return 0;
}
