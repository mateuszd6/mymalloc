#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/mman.h>

#include "timer.h"
#include "../util.h"

static void
shuffle(void** array, int n)
{
    if (n <= 1)
        return;

    for (int i = 0; i < n - 1; i++)
    {
        int j = i + rand() / (RAND_MAX / (n - i) + 1);
        void* t = array[j];
        array[j] = array[i];
        array[i] = t;
    }
}

typedef struct config config;
struct config
{
    char* title;
    i32 n_allocs; /* Must be a multiplication of 1000 */
    i32 n_reps;

    /* size_prob[i] means a probability (in precentiles) of allocating memory of
     * size between 2^i and 2^(i+1). Must sum up to 1000 */
    i16 size_prob[64];
};

#if (TEST == 1)
static config c =
{
    .title = "BALANCED_1",
    .n_allocs = 5000,
    .n_reps = 30 * 1000,
    .size_prob = {
        [4] = 325,
        [5] = 325,
        [6] = 200,
        [7] = 75,
        [8] = 25,
        [9] = 9,
        [10] = 9,
        [11] = 9,
        [12] = 5,
        [13] = 5,
        [14] = 5,
        [15] = 5,
        [16] = 1,
        [17] = 1,
        [18] = 1,
    }
};

#elif (TEST == 2)
static config c =
{
    .title = "STRESS_16B",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [4] = 980,
        [5] = 10,
        [6] = 5,
        [7] = 5,
    }
};

#elif (TEST == 3)
static config c =
{
    .title = "STRESS_32B",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [4] = 30,
        [5] = 950,
        [6] = 10,
        [7] = 5,
        [8] = 5,
    }
};

#elif (TEST == 4)
static config c =
{
    .title = "STRESS_64B",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [4] = 15,
        [5] = 15,
        [6] = 950,
        [7] = 10,
        [8] = 5,
        [9] = 5,
    }
};

#elif (TEST == 5)
static config c =
{
    .title = "STRESS_128B",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [5] = 15,
        [6] = 15,
        [7] = 950,
        [8] = 10,
        [9] = 5,
        [10] = 5,
    }
};

#elif (TEST == 6)
static config c =
{
    .title = "STRESS_256B",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [6] = 15,
        [7] = 15,
        [8] = 950,
        [9] = 10,
        [10] = 5,
        [11] = 5,
    }
};

#elif (TEST == 7)
static config c =
{
    .title = "STRESS_512B",
    .n_allocs = 1000,
    .n_reps =  128 * 1024,
    .size_prob = {
        [7] = 15,
        [8] = 15,
        [9] = 950,
        [10] = 10,
        [11] = 5,
        [12] = 5,
    }
};

#elif (TEST == 8)
static config c =
{
    .title = "STRESS_1KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [8] = 15,
        [9] = 15,
        [10] = 950,
        [11] = 10,
        [12] = 5,
        [13] = 5,
    }
};

#elif (TEST == 9)
static config c =
{
    .title = "STRESS_2KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [9] = 15,
        [10] = 15,
        [11] = 950,
        [12] = 10,
        [13] = 5,
        [14] = 5,
    }
};

#elif (TEST == 10)
static config c =
{
    .title = "STRESS_4KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [10] = 15,
        [11] = 15,
        [12] = 950,
        [13] = 10,
        [14] = 5,
        [15] = 5,
    }
};

#elif (TEST == 11)
static config c =
{
    .title = "STRESS_8KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [11] = 15,
        [12] = 15,
        [13] = 950,
        [14] = 10,
        [15] = 5,
        [16] = 5,
    }
};

#elif (TEST == 12)
static config c =
{
    .title = "STRESS_16KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [12] = 15,
        [13] = 15,
        [14] = 950,
        [15] = 10,
        [16] = 5,
        [17] = 5,
    }
};

#elif (TEST == 13)
static config c =
{
    .title = "STRESS_32KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [13] = 15,
        [14] = 15,
        [15] = 950,
        [16] = 10,
        [17] = 5,
        [18] = 5,
    }
};

#elif (TEST == 14)
static config c =
{
    .title = "STRESS_64KB",
    .n_allocs = 1000,
    .n_reps = 128 * 1024,
    .size_prob = {
        [14] = 15,
        [15] = 15,
        [16] = 950,
        [17] = 10,
        [18] = 5,
        [19] = 5,
    }
};

#elif (TEST == 15)
static config c =
{
    .title = "STRESS_128KB",
    .n_allocs = 1000,
    .n_reps = 1 * 1024,
    .size_prob = {
        [15] = 15,
        [16] = 15,
        [17] = 950,
        [18] = 10,
        [19] = 5,
        [20] = 5,
    }
};

#elif (TEST == 16)
static config c =
{
    .title = "STRESS_256KB",
    .n_allocs = 1000,
    .n_reps = 1 * 1024,
    .size_prob = {
        [16] = 15,
        [17] = 15,
        [18] = 950,
        [19] = 10,
        [20] = 5,
        [21] = 5,
    }
};

#elif (TEST == 17)
static config c =
{
    .title = "STRESS_512KB",
    .n_allocs = 1000,
    .n_reps = 1 * 1024,
    .size_prob = {
        [17] = 15,
        [18] = 15,
        [19] = 950,
        [20] = 10,
        [21] = 5,
        [22] = 5,
    }
};

#elif (TEST == 18)
static config c =
{
    .title = "STRESS_1MB",
    .n_allocs = 1000,
    .n_reps = 1 * 1024,
    .size_prob = {
        [18] = 10,
        [19] = 10,
        [20] = 975,
        [21] = 5,
    }
};

#else
#  error "BAD TEST"
#endif

typedef struct blk blk;
struct blk
{
    char* ptr;
    i64 size;
};

int blk_compar(void const* lv, void const* rv)
{
    i64 diff = ((size_t) ((blk*) lv)->ptr) - ((size_t) ((blk*) rv)->ptr);
    return diff == 0 ? 0 : (diff < 0 ? -1 : 1);
}

int
main()
{
    srand(0xbfba12c);

    /* Allocate them with mmap just to avoid running an allocator and confisuing
     * benchmarks*/
    int prot = PROT_READ | PROT_WRITE;
    int flags = MAP_PRIVATE | MAP_ANONYMOUS;
    void* mem = mmap(0, c.n_allocs * (sizeof(void*) + sizeof(i64) + sizeof(blk)), prot, flags, -1, 0);
    void** ptrs = mem;
    i64* sizes = mem + c.n_allocs * sizeof(void*);
    blk* blks = mem + c.n_allocs * (sizeof(void*) + sizeof(i64));

    i32 total_prob = 0;
    for (i32 i = 0; i < 64; ++i)
        total_prob += c.size_prob[i];
    if (total_prob != 1000)
    {
        fprintf(stderr, "Total probabilities sum to %d, should be 1000!\n", total_prob);
        exit(1);
    }

    i32 sidx = 0;
    for (i32 r = 0; r < c.n_allocs / 1000; ++r)
        for (i32 sz = 0; sz < 64; ++sz)
        {
            for (i32 i = 0; i < c.size_prob[sz]; ++i)
                sizes[sidx++] = 1 << sz;
        }

    shuffle((void**)sizes, c.n_allocs);

    timer t = {0};
    double malloc_time = 0;
    double free_time = 0;
    for (i32 r = 0; r < c.n_reps; ++r)
    {
        TIMER_START(t);
        for (i32 i = 0; i < c.n_allocs; ++i)
        {
            ptrs[i] = malloc(sizes[i]);
            blks[i].ptr = ptrs[i];
            blks[i].size = sizes[i];
            memset(ptrs[i], 0xF7, sizes[i]);
            /* ((char*) ptrs[i])[0] = 'a'; */
        }
        TIMER_STOP(t);
        malloc_time += (double) timer_diff(&t) / (1000 * 1000 * 1000);

        // Check if any blocks interesect. This would mean bug in the allocator
        qsort(blks, c.n_allocs, sizeof(blk), blk_compar);
        for (int i = 0; i < c.n_allocs - 1; ++i)
            if (((size_t) blks[i].ptr) + blks[i].size > ((size_t) blks[i + 1].ptr))
            {
                fprintf(stderr, "BAD!\n");
                exit(1);
            }

        // Free them in random order
        shuffle((void**)ptrs, c.n_allocs);

        TIMER_START(t);
        for (i32 i = 0; i < c.n_allocs; ++i)
        {
            free(ptrs[i]);
        }
        TIMER_STOP(t);
        free_time += (double) timer_diff(&t) / (1000 * 1000 * 1000);
    }

    munmap(mem, c.n_allocs * (sizeof(void*) + sizeof(i64)));
    fprintf(stdout, "%s,%.3lf,%.3lf,%.3lf\n",
            c.title,
            malloc_time + free_time,
            malloc_time * (1000 * 1000 * 1000) / (c.n_reps * c.n_allocs),
            free_time * (1000 * 1000 * 1000) /  (c.n_reps * c.n_allocs));

    return 0;
}
