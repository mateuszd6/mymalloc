#include <stdlib.h>
#include <time.h>

static void CHK_PTR(char* p, int len)
{
    volatile char c = 1;
    for (int i = 0; i < len; ++i)
    {
        c = p[i];
        p[i] = c;
    }
}

int main(void)
{
    srand(0x9f128a4);

#if 0
    for (int i = 0;; ++i)
    {
        size_t size = 262132;
        char* bar = malloc(size);
        CHK_PTR(bar, size);
    }
#endif

    char* foo = 0;
    for (int i = 0; i < 1024 * 1024; ++i)
    {
        /* if (i == 148) */
        /*     printf("Here\n"); */

        /* Make a big allocation */
        if (i % 2048 == 0)
        {
            int size = 512 * 1024 + (20 * (rand() % 100));
            foo = malloc(size);
            CHK_PTR(foo, size);
            continue;
        }


        int size = (1 << ((rand() % 16) + 4)) - (rand() % 13);
        /* printf("size: %d\n", size); */
        foo = malloc(size);
        CHK_PTR(foo, size);
    }

    return 0;
}
