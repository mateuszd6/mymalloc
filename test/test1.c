#include <stdlib.h>
#include "../main.c"

static void CHK_PTR(char* p, int len)
{
    volatile char c = 1;
    for (int i = 0; i < len; ++i)
    {
        c = p[i];
        c++;
        c--;
        p[i] = c;
    }

    c == -1 ? (void) p : (void) p;
}

int main(void)
{
    char* foo = 0;
    int i = 0;

    for (;; ++i)
    {
        if (i == 20404)
        {
            foo = 0;
        }

        foo = my_malloc(16);
        CHK_PTR(foo, 16);
        printf("i = %d\n", i);
    }

    return 0;
}
