#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "timer.h"

// #include "../main.c"

#define NALLOCS (800)
#define NREPS (128 * 1024)
#define ALLOCATION_SIZE 30 // 16 * 4096

static void
shuffle(void** array, int n)
{
    if (n <= 1)
        return;

    for (int i = 0; i < n - 1; i++)
    {
        int j = i + rand() / (RAND_MAX / (n - i) + 1);
        void* t = array[j];
        array[j] = array[i];
        array[i] = t;
    }
}

int main()
{
    srand(0xf81294a7);
    double total_time = 0.0f;
    timer t = {0};
    for (int r = 0; r < NREPS; ++r)
    {
        char* p[NALLOCS];
        /* TIMER_START(t); */
        for (int i = 0; i < NALLOCS; ++i)
        {
            p[i] = malloc(ALLOCATION_SIZE);
            p[i][0] = 'a';
            p[i][2] = 'b';
        }
        /* TIMER_STOP(t); */
        /* total_time += (double) timer_diff(&t) / (1000 * 1000 * 1000); */

        shuffle((void**) p, NALLOCS);

        TIMER_START(t);
        for (int i = 0; i < NALLOCS - 10; ++i)
        {
            free((void*) p[i]);
        }
        TIMER_STOP(t);
        total_time += (double) timer_diff(&t) / (1000 * 1000 * 1000);
    }
    fprintf(stdout, "Elapsed: %.3lfs\n", total_time);

    return 0;
}
