#include <stdlib.h>
#include <stdio.h>

#include "timer.h"

#include "../main.c"

#define NALLOCS (8000)
#define NREPS 1 // (128 * 1024 / 8)

int main()
{
    // char* c0 = malloc(7);
    // char* c1 = malloc(8);
    // char* c2 = malloc(15);
    // char* c4 = malloc(16);
    // char* c5 = malloc(30);
    // char* c6 = malloc(32);
    // char* c7 = malloc(33);
    // char* c8 = malloc(63);
    // char* c9 = malloc(64);

    timer t = {0};
    TIMER_START(t);
    for (int r = 0; r < NREPS; ++r)
    {
        char volatile* p[NALLOCS];
        for (int i = 0; i < NALLOCS; ++i)
        {
            p[i] = my_malloc(30);
            p[i][0] = 'a';
            p[i][2] = 'b';
        }

        for (int i = 0; i < NALLOCS; ++i)
        {
            my_free((void*) p[i]);
        }
    }
    TIMER_STOP(t);
    fprintf(stdout, "Elapsed: %.3lfs\n", (double) timer_diff(&t) / (1000 * 1000 * 1000));

    return 0;
}
