#!/bin/bash

function prelude() {
echo "#include <benchmark/benchmark.h>"
echo ""
echo "#define SHELL_BENCH(NAME, COMMAND)                                              \\"
echo "static void NAME (benchmark::State& state)                                      \\"
echo "{                                                                               \\"
echo "    for (auto _ : state)                                                        \\"
echo "    {                                                                           \\"
echo "        int ret = system(COMMAND);                                              \\"
echo "        state.PauseTiming();                                                    \\"
echo "        if (__builtin_expect(ret != 0, 0))                                      \\"
echo "        {                                                                       \\"
echo "            char buf[128];                                                      \\"
echo "            sprintf(buf, \"Command failed with code %d\", ret);                   \\"
echo "            state.SkipWithError(buf);                                           \\"
echo "            return;                                                             \\"
echo "        }                                                                       \\"
echo "        state.ResumeTiming();                                                   \\"
echo "    }                                                                           \\"
echo "}                                                                               \\"
echo "BENCHMARK(NAME);"
}

function epilogue() {
    echo "BENCHMARK_MAIN();"
}

# ------------------------------------------------------------------------------------

SRCNAME=`mktemp --suffix=.cpp`
OUTNAME=`mktemp`

prelude >> $SRCNAME

while [ -n "$1" ]
do
    echo "SHELL_BENCH($1, \"$2\")" >> $SRCNAME
    shift 2
done

epilogue >> $SRCNAME

g++ $SRCNAME -o $OUTNAME -lbenchmark -O3

# TODO: don't inclue sudo password, lol!
echo "mateusz121212" | sudo -S cpupower frequency-set --governor performance > /dev/null
$OUTNAME
echo "mateusz121212" | sudo -S cpupower frequency-set --governor powersave > /dev/null

rm $SRCNAME $OUTNAME
