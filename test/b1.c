#include <stdlib.h>
#include <stdio.h>

#include "timer.h"

// #include "../main.c"

#define NALLOCS (800)
#define NREPS (1)
#define ALLOCATION_SIZE 30 // 4096

int main()
{
    // char* c0 = malloc(7);
    // char* c1 = malloc(8);
    // char* c2 = malloc(15);
    // char* c4 = malloc(16);
    // char* c5 = malloc(30);
    // char* c6 = malloc(32);
    // char* c7 = malloc(33);
    // char* c8 = malloc(63);
    // char* c9 = malloc(64);

    timer t = {0};
    TIMER_START(t);
    for (int r = 0; r < NREPS; ++r)
    {
        char* p[NALLOCS];
        for (int i = 0; i < NALLOCS; ++i)
        {
#if 0
            /* Causes problems for small pages 4096? */
            if (r == 0 && i == 15)
            {
                int qq = 0;
                (void) qq;
            }
#endif

            p[i] = malloc(ALLOCATION_SIZE);
            p[i][0] = 'a';
            p[i][2] = 'b';
        }

        for (int i = 0; i < NALLOCS; ++i)
        {
            free((void*) p[i]);
        }
    }
    TIMER_STOP(t);
    fprintf(stdout, "Elapsed: %.3lfs\n", (double) timer_diff(&t) / (1000 * 1000 * 1000));

    return 0;
}
