#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "../main.c"

#define NALLOCS (3000)
#define ALLOCATION_SIZE (16 * 1024)

static char* ptrs[NALLOCS];

int
main()
{
    for (int r = 0; r < NALLOCS; r++)
    {
        ptrs[r] = my_malloc(ALLOCATION_SIZE);
        ptrs[r][0] = 'a';
        ptrs[r][1] = 'b';
        ptrs[r][2] = 'c';
        ptrs[r][3] = 0;
    }

    heap* hl = &heap_local;
    (void) hl;

    for (int i = 0; i < NALLOCS / 2; ++i)
        my_free(ptrs[i]);

    /* my_free(ptrs[1022 + 0]); */
    /* my_free(ptrs[1022 + 1]); */
    /* my_free(ptrs[1022 + 2]); */

#if 0
    for (int i = 0; i < 2038 - 886; ++i)
    {
        char* x = my_malloc(30);
        x[0] = 'a';
        x[1] = 'b';
        x[2] = 'c';
        x[3] = 0;
    }
#endif

    char* y = my_malloc(4 * 1024 - 7);
    (void) y;

    char* z = my_malloc(513 * 1024);
    *(int*)z = 0xFFEEDDCC;
    my_free(z);

    return 0;
}
