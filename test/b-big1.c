#include <stdlib.h>
#include <stdio.h>

#include "timer.h"

#include "../main.c"

#define NALLOCS 1 // (40)
#define NREPS 1 // (128 * 1024)

int main()
{
    timer t = {0};
    TIMER_START(t);
    for (int r = 0; r < NREPS; ++r)
    {
        char volatile* p[NALLOCS];
        for (int i = 0; i < NALLOCS; ++i)
        {
            p[i] = my_malloc(12 * 1024 * 1024 );
            p[i][0] = 'a';
            p[i][2] = 'b';
        }

        for (int i = 0; i < NALLOCS; ++i)
        {
            my_free((void*) p[i]);
        }
    }
    TIMER_STOP(t);
    fprintf(stdout, "Elapsed: %.3lfs\n", (double) timer_diff(&t) / (1000 * 1000 * 1000));

    return 0;
}
