#include <benchmark/benchmark.h>

#define MI_SO "env LD_PRELOAD=/home/mateusz/sandbox/mimalloc/out/release/libmimalloc.so"
#define MY_SO "env LD_PRELOAD=/home/mateusz/work/sova/code/my_malloc.so"
#define JE_SO "env LD_PRELOAD=/home/mateusz/sandbox/jemalloc/lib/libjemalloc.so"
#define TC_SO "env LD_PRELOAD=/home/mateusz/sandbox/gperftools/.libs/libtcmalloc.so"

#define LATC_PATH "/home/mateusz/work/latte/"

#define SHELL_BENCH(NAME, COMMAND)                                              \
static void NAME (benchmark::State& state)                                      \
{                                                                               \
    for (auto _ : state)                                                        \
    {                                                                           \
        int ret = system(COMMAND);                                              \
        state.PauseTiming();                                                    \
        if (__builtin_expect(ret != 0, 0))                                      \
        {                                                                       \
            char buf[128];                                                      \
            sprintf(buf, "Command failed with code %d", ret);                   \
            state.SkipWithError(buf);                                           \
            return;                                                             \
        }                                                                       \
        state.ResumeTiming();                                                   \
    }                                                                           \
}                                                                               \
BENCHMARK(NAME);

SHELL_BENCH(LATC_GLIBC, LATC_PATH "./latc " LATC_PATH "./big-test1200.lat")
SHELL_BENCH(LATC_MI, MI_SO " " LATC_PATH "./latc " LATC_PATH "./big-test1200.lat")
SHELL_BENCH(LATC_MY, MY_SO " " LATC_PATH "./latc " LATC_PATH "./big-test1200.lat")
SHELL_BENCH(LATC_JE, JE_SO " " LATC_PATH "./latc " LATC_PATH "./big-test1200.lat")
SHELL_BENCH(LATC_TC, TC_SO " " LATC_PATH "./latc " LATC_PATH "./big-test1200.lat")

int main(int argc, char** argv)
{
    ::benchmark::Initialize(&argc, argv);
    ::benchmark::RunSpecifiedBenchmarks();

    return 0;
}
