#!/bin/sh

export TIMEFORMAT=%R

export MI_SO="/home/mateusz/sandbox/mimalloc/out/release/libmimalloc.so"
export ME_SO="/home/mateusz/work/sova/code/my_malloc.so"
export JE_SO="/home/mateusz/sandbox/jemalloc/lib/libjemalloc.so"
export TC_SO="/home/mateusz/sandbox/gperftools/.libs/libtcmalloc.so"

export LATC_PATH="/home/mateusz/work/latte/"
export CFRAC="/home/mateusz/cfrac"

export BENCHCMD=`realpath ./shellbench.sh` # To allow execution in different wd
export MEASURE_MEM='time -f "%M"'

$BENCHCMD \
    "BNFC_GNU" "./bnfc/Testjava ./bnfc/foobar.java" \
    "BNFC_MI" "env LD_PRELOAD=${MI_SO} ./bnfc/Testjava ./bnfc/foobar.java" \
    "BNFC_ME" "env LD_PRELOAD=${ME_SO} ./bnfc/Testjava ./bnfc/foobar.java" \
    "BNFC_JE" "env LD_PRELOAD=${JE_SO} ./bnfc/Testjava ./bnfc/foobar.java" \
    "BNFC_TC" "env LD_PRELOAD=${TC_SO} ./bnfc/Testjava ./bnfc/foobar.java"

# MEMORY
echo "BFNC MEM:"
$MEASURE_MEM ./bnfc/Testjava ./bnfc/foobar.java
$MEASURE_MEM env LD_PRELOAD=${MI_SO} ./bnfc/Testjava ./bnfc/foobar.java
$MEASURE_MEM env LD_PRELOAD=${ME_SO} ./bnfc/Testjava ./bnfc/foobar.java
$MEASURE_MEM env LD_PRELOAD=${JE_SO} ./bnfc/Testjava ./bnfc/foobar.java
$MEASURE_MEM env LD_PRELOAD=${TC_SO} ./bnfc/Testjava ./bnfc/foobar.java

exit 1

# $BENCHCMD \
    # "CFRAC_GNU" "${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null" \
    # "CFRAC_MI" "env LD_PRELOAD=${MI_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null" \
    # "CFRAC_ME" "env LD_PRELOAD=${ME_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null" \
    # "CFRAC_JE" "env LD_PRELOAD=${JE_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null" \
    # "CFRAC_TC" "env LD_PRELOAD=${TC_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null"
# MEMORY
# echo "CFRAC MEM:"
# $MEASURE_MEM ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${MI_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${ME_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${JE_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${TC_SO} ${CFRAC} 17545186520507317056371138836327483792789528 > /dev/null

# echo -n "GNU: "
# time ./mstress > /dev/null
# echo -n "MI: "
# time env LD_PRELOAD=${MI_SO} ./mstress > /dev/null
# echo -n "ME: "
# time env LD_PRELOAD=${ME_SO} ./mstress > /dev/null
# echo -n "JE: "
# time env LD_PRELOAD=/home/mateusz/sandbox/jemalloc/lib/libjemalloc.so ./mstress > /dev/null
# echo -n "TC: "
# time env LD_PRELOAD=${TC_SO} ./mstress > /dev/null
# echo ""
# exit

# GNU SIMPLE
# TIME:
# no time command, because this bench prints time on its own
# echo -n "GNU: "
# time -v ./bench-malloc-simple # > /dev/null
# echo -n "MI: "
# time -v env LD_PRELOAD=${MI_SO} ./bench-malloc-simple # > /dev/null
# echo -n "ME: "
# time -v env LD_PRELOAD=${ME_SO} ./bench-malloc-simple # > /dev/null
# echo -n "JE: "
# time -v env LD_PRELOAD=${JE_SO} ./bench-malloc-simple # > /dev/null
# echo -n "TC: "
# time -v env LD_PRELOAD=${TC_SO} ./bench-malloc-simple # > /dev/null
# echo ""
#
# MEMORY:
# echo "GNU SIMPLE MEM:"
# echo -n "GNU: "
# $MEASURE_MEM ./bench-malloc-simple > /dev/null
# echo -n "MI: "
# $MEASURE_MEM env LD_PRELOAD=${MI_SO} ./bench-malloc-simple > /dev/null
# echo -n "ME: "
# $MEASURE_MEM env LD_PRELOAD=${ME_SO} ./bench-malloc-simple > /dev/null
# echo -n "JE: "
# $MEASURE_MEM env LD_PRELOAD=${JE_SO} ./bench-malloc-simple > /dev/null
# echo -n "TC: "
# $MEASURE_MEM env LD_PRELOAD=${TC_SO} ./bench-malloc-simple > /dev/null
# echo ""
# exit

# no time command, because this bench prints time on its own
# echo -n "GNU: "
# ./bench-malloc-thread 8 # > /dev/null
# echo -n "MI: "
# env LD_PRELOAD=${MI_SO} ./bench-malloc-thread 8 # > /dev/null
# echo -n "ME: "
# env LD_PRELOAD=${ME_SO} ./bench-malloc-thread 8 # > /dev/null
# echo -n "JE: "
# env LD_PRELOAD=${JE_SO} ./bench-malloc-thread 8 # > /dev/null
# echo -n "TC: "
# env LD_PRELOAD=${TC_SO} ./bench-malloc-thread 8 # > /dev/null
# echo ""
# exit
# echo "GNU THREAD MEM:"
# echo -n "GNU: "
# $MEASURE_MEM ./bench-malloc-thread 8 > /dev/null
# echo -n "MI: "
# $MEASURE_MEM env LD_PRELOAD=${MI_SO} ./bench-malloc-thread 8 > /dev/null
# echo -n "ME: "
# $MEASURE_MEM env LD_PRELOAD=${ME_SO} ./bench-malloc-thread 8 > /dev/null
# echo -n "JE: "
# $MEASURE_MEM env LD_PRELOAD=${JE_SO} ./bench-malloc-thread 8 > /dev/null
# echo -n "TC: "
# $MEASURE_MEM env LD_PRELOAD=${TC_SO} ./bench-malloc-thread 8 > /dev/null
# echo ""

# $BENCHCMD \
    # "CS_GNU" "./cache-scratch 1 1000 1 2000000 8 > /dev/null" \
    # "CS_MI" "env LD_PRELOAD=${MI_SO} ./cache-scratch 1 1000 1 2000000 8 > /dev/null" \
    # "CS_ME" "env LD_PRELOAD=${ME_SO} ./cache-scratch 1 1000 1 2000000 8 > /dev/null" \
    # "CS_JE" "env LD_PRELOAD=${JE_SO} ./cache-scratch 1 1000 1 2000000 8 > /dev/null" \
    # "CS_TC" "env LD_PRELOAD=${TC_SO} ./cache-scratch 1 1000 1 2000000 8 > /dev/null"

# $BENCHCMD \
    # "AT1_GNU" "./alloc-test/alloc-test 1" \
    # "AT1_MI" "env LD_PRELOAD=${MI_SO} ./alloc-test/alloc-test 1" \
    # "AT1_ME" "env LD_PRELOAD=${ME_SO} ./alloc-test/alloc-test 1" \
    # "AT1_JE" "env LD_PRELOAD=${JE_SO} ./alloc-test/alloc-test 1" \
    # "AT1_TC" "env LD_PRELOAD=${TC_SO} ./alloc-test/alloc-test 1"

$BENCHCMD \
    "ML_GNU" "./malloc-large/malloc-large" \
    "ML_MI" "env LD_PRELOAD=${MI_SO} ./malloc-large/malloc-large" \
    "ML_ME" "env LD_PRELOAD=${ME_SO} ./malloc-large/malloc-large" \
    "ML_JE" "env LD_PRELOAD=${JE_SO} ./malloc-large/malloc-large" \
    "ML_TC" "env LD_PRELOAD=${TC_SO} ./malloc-large/malloc-large"
echo "alloc-large MEM:"
$MEASURE_MEM ./malloc-large/malloc-large
$MEASURE_MEM env LD_PRELOAD=${MI_SO} ./malloc-large/malloc-large
$MEASURE_MEM env LD_PRELOAD=${ME_SO} ./malloc-large/malloc-large
$MEASURE_MEM env LD_PRELOAD=${JE_SO} ./malloc-large/malloc-large
$MEASURE_MEM env LD_PRELOAD=${TC_SO} ./malloc-large/malloc-large

# $BENCHCMD \
    # "RS_GNU" "ruby ./rbstress/stress_mem.rb 8" \
    # "RS_MI" "env LD_PRELOAD=${MI_SO} ruby ./rbstress/stress_mem.rb 8" \
    # "RS_ME" "env LD_PRELOAD=${ME_SO} ruby ./rbstress/stress_mem.rb 8" \
    # "RS_JE" "env LD_PRELOAD=${JE_SO} ruby ./rbstress/stress_mem.rb 8" \
    # "RS_TC" "env LD_PRELOAD=${TC_SO} ruby ./rbstress/stress_mem.rb 8"

# $BENCHCMD \
    # "AT8_GNU" "./alloc-test/alloc-test 8" \
    # "AT8_MI" "env LD_PRELOAD=${MI_SO} ./alloc-test/alloc-test 8" \
    # "AT8_ME" "env LD_PRELOAD=${ME_SO} ./alloc-test/alloc-test 8" \
    # "AT8_JE" "env LD_PRELOAD=${JE_SO} ./alloc-test/alloc-test 8" \
    # "AT8_TC" "env LD_PRELOAD=${TC_SO} ./alloc-test/alloc-test 8"
# MEMORY:
# echo "alloc-test 8 MEM:"
# $MEASURE_MEM ./alloc-test/alloc-test 8
# $MEASURE_MEM env LD_PRELOAD=${MI_SO} ./alloc-test/alloc-test 8
# $MEASURE_MEM env LD_PRELOAD=${ME_SO} ./alloc-test/alloc-test 8
# $MEASURE_MEM env LD_PRELOAD=${JE_SO} ./alloc-test/alloc-test 8
# $MEASURE_MEM env LD_PRELOAD=${TC_SO} ./alloc-test/alloc-test 8

# $BENCHCMD \
    # "Z3_GNU" "z3 -smt2 /home/mateusz/test1.smt2 > /dev/null" \
    # "Z3_MI" "env LD_PRELOAD=${MI_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null" \
    # "Z3_ME" "env LD_PRELOAD=${ME_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null" \
    # "Z3_JE" "env LD_PRELOAD=${JE_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null" \
    # "Z3_TC" "env LD_PRELOAD=${TC_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null"
# MEMORY:
# echo "z3 MEM:"
# $MEASURE_MEM z3 -smt2 /home/mateusz/test1.smt2 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${MI_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${ME_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${JE_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null
# $MEASURE_MEM env LD_PRELOAD=${TC_SO} z3 -smt2 /home/mateusz/test1.smt2 > /dev/null

# pushd . &> /dev/null
# cd /home/mateusz/sandbox/lean/library/init/algebra/
# echo -n "LEAN_GNU: "
# find . -name "*.olean" -type f -delete
# time ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_MI: "
# find . -name "*.olean" -type f -delete
# time env LD_PRELOAD=${MI_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_ME: "
# find . -name "*.olean" -type f -delete
# time env LD_PRELOAD=${ME_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_JE: "
# find . -name "*.olean" -type f -delete
# time env LD_PRELOAD=${JE_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_TC: "
# find . -name "*.olean" -type f -delete
# time env LD_PRELOAD=${TC_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo ""
# popd &> /dev/null
#MEMORY:
# echo "lean MEM:"
# pushd . &> /dev/null
# cd /home/mateusz/sandbox/lean/library/init/algebra/
# echo -n "LEAN_GNU: "
# find . -name "*.olean" -type f -delete
# $MEASURE_MEM ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_MI: "
# find . -name "*.olean" -type f -delete
# $MEASURE_MEM env LD_PRELOAD=${MI_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_ME: "
# find . -name "*.olean" -type f -delete
# $MEASURE_MEM env LD_PRELOAD=${ME_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_JE: "
# find . -name "*.olean" -type f -delete
# $MEASURE_MEM env LD_PRELOAD=${JE_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo -n "LEAN_TC: "
# find . -name "*.olean" -type f -delete
# $MEASURE_MEM env LD_PRELOAD=${TC_SO} ../../../bin/lean --make -j 8 > /dev/null
# echo ""
# popd &> /dev/null

# for i in {1..18}; do
    # clang -Wall -Wextra -Wshadow -Wno-unused-function -O3 -g0 -DTEST=${i} -march=native ./stress.c -o ./stress
    # echo -n "GNU,"
    # ./stress
    # echo -n "MI,"
    # LD_PRELOAD=${MI_SO} ./stress
    # echo -n "ME,"
    # LD_PRELOAD=${ME_SO} ./stress
    # echo -n "JE,"
    # LD_PRELOAD=${JE_SO} ./stress
    # echo -n "TC,"
    # LD_PRELOAD=${TC_SO} ./stress
    # echo ",,"
# done
