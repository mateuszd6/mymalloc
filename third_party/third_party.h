// This code is a part of Microsofts' mimalloc
// https://www.github.com/microsoft/mimalloc
//
// MIT License
//
// Copyright (c) 2019 Microsoft Corporation, Daan Leijen
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

static inline void*
mi_tls_slot(size_t slot)
{
    void* res;
    const size_t ofs = (slot * sizeof(void*));
    __asm__("movq %%fs:%1, %0" : "=r" (res) : "m" (*((void**)ofs)) : );  // x86_64 Linux, BSD uses FS

    return res;
}

typedef struct list_head list_head;
struct list_head
{
    list_head* next;
};

static inline void
atomic_push(list_head** list, list_head* block)
{
    do
    {
        block->next = *list;
    } while (!__sync_bool_compare_and_swap(list, block->next, block));
}

static inline void
atomic_inc(int* ptr)
{
    __sync_fetch_and_add(ptr, 1);
}

static inline void
atomic_sub(int* ptr, int val)
{
    int prev = __sync_fetch_and_sub(ptr, val);
    (void) prev;
    assert(prev - val >= 0);
}

static inline void*
atomic_ptr_swap(void** ptr, void* val)
{
    void* ret;
    __atomic_exchange(ptr, &val, &ret, __ATOMIC_RELAXED);
    return ret;
}
