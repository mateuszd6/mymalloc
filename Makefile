CC := gcc
CCX := g++

CFLAGS_DEBUG := -DDEBUG -O0 -g3
CFLAGS_RELEASE := -O3 -g0 -march=native
CFLAGS_PROFILE := -fno-omit-frame-pointer -O3 -g1 -march=native
CFLAGS := $(CFLAGS_RELEASE)

CWARN := -Wall -Wextra -Wshadow -Wno-unused-function

all:
	$(CC) -fPIC -shared $(CFLAGS) $(CWARN) main.c -o my_malloc.so
	$(CC) $(CWARN) $(CFLAGS) ./test/test2.c -o ./test/test2
	$(CC) $(CWARN) $(CFLAGS) ./test/test3.c -o ./test/test3
	$(CC) $(CWARN) $(CFLAGS) ./test/b0.c -o ./test/b0
	$(CC) $(CWARN) $(CFLAGS) ./test/b1.c -o ./test/b1
	$(CC) $(CWARN) $(CFLAGS) ./test/b2.c -o ./test/b2
	$(CC) $(CWARN) $(CFLAGS) ./test/b3.c -o ./test/b3
	$(CC) $(CWARN) $(CFLAGS) ./test/b-big1.c -o ./test/b-big1
	$(CC) $(CWARN) $(CFLAGS) ./test/b-thread1.c -o ./test/b-thread1 -lpthread
	$(CC) $(CWARN) $(CFLAGS) -DTEST=18 ./test/stress.c -o ./test/stress
