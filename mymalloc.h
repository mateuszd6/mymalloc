#ifndef SEGMT_H_
#define SEGMT_H_

#include <stdatomic.h>

#include "util.h"

#define SHIFT_TO_PAGE_SMALL (16)
#define SHIFT_TO_PAGE_LARGE (22)
#define SHIFT_TO_PAGE_VERY_LARGE (22)

#define SEGMT_TYPE_SMALL (1)
#define SEGMT_TYPE_LARGE (2)
#define SEGMT_TYPE_VERY_LARGE (3)

#define PAGE_SIZE (64 * 1024)
#define SEGMENT_SIZE (4ULL * 1024 * 1024)
#define NUM_PAGES_IN_SEGMENT (64)

#define MAX_SMALL_ALLOC_SIZE (512)
#define MAX_LARGE_ALLOC_SIZE (512 * 1024)

/* How much of the max number of used pages should we cache? */
#define MAX_SMALL_FRACT_CACHE (8)
#define MAX_BIG_FRACT_CACHE (8)

/* TODO: Should be able to subtract 4. Check that! */
#define NUM_PAGES (16 + 48) /* [16 - 512K] + 48 zeros for very large sizes */

typedef struct page page;
struct page
{
    page* next; /* Used both in the cache and in the main allocator */
    page* prev; /* Used only in size queues, rest of the queues (cache, etc) are singly-linked */
    i16 pad_1;
    i16 pad_2;
    i32 pad_3;
    i32 pad_4;
    i32 thread_freed;
    void* thread_free;
    void* pad_5;

    i32 used;
    i32 size; /* TODO: This actually doesn't allow us to allocate sizes larger than 2^32 */
    u64 meta[2]; /* expands further */
};

typedef struct segmt segmt;
struct segmt
{
    page* next;
    page* prev;
    i8 shift_to_page; /* How many bits to mask to convert ptr to page base */
    i8 type; /* small / large / very large */
    i16 pages_in_segment;
    i32 pad_1;
    i32 pad_2;
    i32 thread_freed;
    void* thread_free;
    void* heap_id; /* unique ptr to a thread_local heap (check owner) */

    i32 used;
    i32 size;
    u64 meta[2]; /* expands further */
};

typedef struct heap heap;
struct heap
{
    i32 small_curr_used; /* num small pages currently in use */
    i32 small_max_used; /* max num small pages ever used */
    page* small_free; /* small pages cache */
    i32 small_cached; /* num cached small pages */
    i32 big_curr_used; /* num big pages currently in use */
    i32 big_max_used; /* max num big pages ever used */
    i32 big_cached; /* num cached big pages */
    page* big_free; /* big pages cache */
    page* curr_block[NUM_PAGES]; /* takes log2 of the size of a pointer */
};
_Thread_local static heap heap_local = {0};

static const struct
{
    u64 meta0;
    u64 meta1;
} g_bitmeta[] =
{
    /* 16                                0123456789012345678901234567890123456 */
    { .meta0 = (u64) 0, .meta1 = (u64) 0b1111111111111111111111111111111111111 },
    // 32
    { .meta0 = (u64) ((u64) -1) << 32, .meta1 = (u64) 0b1111111111 },
    // 64
    { .meta0 = (u64) ((u64) -1) << 16, .meta1 = (u64) 0b111 },
    // 128
    { .meta0 = (u64) ((u64) -1) << 8, .meta1 = (u64) 0b1 },
    // 256
    { .meta0 = (u64) ((u64) -1) << 4, .meta1 = (u64) 0b1 },
    // 512
    { .meta0 = (u64) ((u64) -1) << 2, .meta1 = (u64) 0b1 },
    // 1k
    { .meta0 = (u64) 0, .meta1 = (u64) 0b1 },
    // 2k
    { .meta0 = (u64) ((u64) -1) << 32, .meta1 = (u64) 0b1 },
    // 4k
    { .meta0 = (u64) ((u64) -1) << 16, .meta1 = (u64) 0b1 },
    // 8k
    { .meta0 = (u64) ((u64) -1) << 8, .meta1 = (u64) 0b1 },
    // 16k
    { .meta0 = (u64) ((u64) -1) << 4, .meta1 = (u64) 0b1 },
    // 32k
    { .meta0 = (u64) ((u64) -1) << 2, .meta1 = (u64) 0b1 },
    // 64k
    { .meta0 = (u64) ((u64) -1) << 1, .meta1 = (u64) 0b1 },
    // 128k                                             01234567890123456789012345678901
    { .meta0 = (u64) ((u64) -1) << 1, .meta1 = (u64) ~0b11111111111111111111111111111110ULL },
    // 256k
    { .meta0 = (u64) ((u64) -1) << 1, .meta1 = (u64) ~0b1111111111111110ULL },
    // 512k
    { .meta0 = (u64) ((u64) -1) << 1, .meta1 = (u64) ~0b11111110ULL },
};

#endif // SEGMT_H_
